package com.btl.sqa_n1_5.utils;

import com.btl.sqa_n1_5.model.entity.NguoiDongThue;
import com.btl.sqa_n1_5.model.entity.ThongTinThue;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class JsonToNguoiDongThueConverter {

    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final String FILE_PATH = "src/main/resources/fake_data.json";
    private static final String THUE_PATH = "src/main/resources/mock_thue.json";
    private static List<NguoiDongThue> convertJsonToNguoiDongThueList(String filePath) {
        try {
            byte[] jsonData = Files.readAllBytes(Paths.get(filePath));
            if (jsonData.length == 0) {
                return new ArrayList<>();
            }
            // Use TypeReference to specify the type of the target object
            List<NguoiDongThue> nguoiDongThueList = objectMapper.readValue(jsonData, new TypeReference<List<NguoiDongThue>>() {});
            return nguoiDongThueList;
        } catch (IOException e) {
            // Handle the exception according to your application's requirements
            e.printStackTrace();
            return Collections.emptyList();
        }
    }


    public static NguoiDongThue findNguoiDongThueByMasothue(String masothue) {
        return convertJsonToNguoiDongThueList(FILE_PATH).stream()
                .filter(nguoiDongThue -> masothue.equals(nguoiDongThue.getMasothue()))
                .findFirst()
                .orElse(null);
    }
    public static NguoiDongThue findNguoiDongThueById(String id) {
        return convertJsonToNguoiDongThueList("src/main/resources/fake_data.json").stream()
                .filter(nguoiDongThue -> id.equals(nguoiDongThue.getThanhvienId()))
                .findFirst()
                .orElse(null);
    }

    private static void writeJsonFile(List<?> nguoiDongThueList, String filePath) {
        try {
            byte[] jsonData = objectMapper.writeValueAsBytes(nguoiDongThueList);
            Files.write(Paths.get(filePath), jsonData);
        } catch (IOException e) {
            // Handle the exception according to your application's requirements
            e.printStackTrace();
        }
    }

    public static void updateTaxStatus(String thanhvienId, int newTaxStatus) {
        List<NguoiDongThue> nguoiDongThueList = convertJsonToNguoiDongThueList(FILE_PATH);

        if (nguoiDongThueList != null) {
            nguoiDongThueList.stream()
                    .filter(nguoiDongThue -> thanhvienId.equals(nguoiDongThue.getThanhvienId()))
                    .forEach(nguoiDongThue -> nguoiDongThue.setTaxStatus(newTaxStatus));

            writeJsonFile(nguoiDongThueList, FILE_PATH);
        } else {
            System.out.println("Failed to update taxStatus. Unable to read JSON file.");
        }
    }

    private static NguoiDongThue readJsonFile(String filePath) {
        try {
            byte[] jsonData = Files.readAllBytes(Paths.get(filePath));
            return objectMapper.readValue(jsonData, NguoiDongThue.class);
        } catch (IOException e) {
            // Handle the exception according to your application's requirements
            e.printStackTrace();
            return null;
        }
    }

    private static List<ThongTinThue> convertJsonToThongTinThueList(String filePath) {
        try {
            byte[] jsonData = Files.readAllBytes(Paths.get(filePath));

            // Check if the JSON data is empty
            if (jsonData.length == 0) {
                return new ArrayList<>();
            }

            // Use TypeReference to specify the type of the target object
            List<ThongTinThue> thongTinThueList = objectMapper.readValue(jsonData, new TypeReference<>() {});
            return thongTinThueList;
        } catch (IOException e) {
            // Handle the exception according to your application's requirements
            e.printStackTrace();
            return Collections.emptyList();
        }
    }


    public static void saveThongTinThue(ThongTinThue thongTinThue) {
        List<ThongTinThue> thongTinThueList = convertJsonToThongTinThueList(THUE_PATH);

        if (thongTinThueList == null) {
            thongTinThueList = new ArrayList<>();
        }

        boolean found = false;
        for (ThongTinThue existingThongTinThue : thongTinThueList) {
            if (thongTinThue.getNguoiDongThueId().equals(existingThongTinThue.getNguoiDongThueId())) {
                // Cập nhật các trường cần thiết
                existingThongTinThue.setId(thongTinThue.getId());
                existingThongTinThue.setNguoiDongThueId(thongTinThue.getNguoiDongThueId());
                existingThongTinThue.setTNCT(thongTinThue.getTNCT());
                existingThongTinThue.setTNPSVN(thongTinThue.getTNPSVN());
                existingThongTinThue.setTNPSQT(thongTinThue.getTNPSQT());
                existingThongTinThue.setDSGT(thongTinThue.getDSGT());
                existingThongTinThue.setGTCN(thongTinThue.getGTCN());
                existingThongTinThue.setGTPT(thongTinThue.getGTPT());
                existingThongTinThue.setTTND(thongTinThue.getTTND());
                existingThongTinThue.setGTBH(thongTinThue.getGTBH());
                existingThongTinThue.setSotienTinhthue(thongTinThue.getSotienTinhthue());
                existingThongTinThue.setThueCanNop(thongTinThue.getThueCanNop());
                existingThongTinThue.setSonguoiphuthuoc(thongTinThue.getSonguoiphuthuoc());

                found = true;
                break;
            }
        }

        if (!found && thongTinThueList != null) {
            thongTinThueList.add(thongTinThue);
        }

        writeJsonFile(thongTinThueList, THUE_PATH);
    }

    public static ThongTinThue findThongTinThueByUserId(String id) {
        return convertJsonToThongTinThueList(THUE_PATH).stream()
                .filter(ttt -> id.equals(ttt.getNguoiDongThueId()))
                .findFirst()
                .orElse(null);
    }

}
