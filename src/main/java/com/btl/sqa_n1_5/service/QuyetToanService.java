package com.btl.sqa_n1_5.service;

import com.btl.sqa_n1_5.model.entity.ThongTinThue;

public interface QuyetToanService {
  ThongTinThue findByUserId(String userid);

}
