package com.btl.sqa_n1_5;

import com.btl.sqa_n1_5.utils.JsonToNguoiDongThueConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class SqaN15Application {

  public static void main(String[] args) {
//    log.info("[nguoi dong thue] {}", JsonToNguoiDongThueConverter.findNguoiDongThueByMasothue("123456"));
//    JsonToNguoiDongThueConverter.updateTaxStatus("2", 4);

    SpringApplication.run(SqaN15Application.class, args);
  }

}
