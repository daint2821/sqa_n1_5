package com.btl.sqa_n1_5.model.entity;

import com.btl.sqa_n1_5.constant.GiamTruConstant;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.PrePersist;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Entity
@Data
public class ThongTinThue {
  @Id
  private String id;
  private String nguoiDongThueId;
  private Long TNCT;
  private Long TNPSVN;
  private Long TNPSQT;
  private Long DSGT;
  private Long GTCN = GiamTruConstant.GiamTruCaNhan;
  private Long GTPT = GiamTruConstant.GiamTruPhuThuoc;
  private Long TTND;
  private Long GTBH;
  private Long SotienTinhthue;
  private Long ThueCanNop;
  private Long songuoiphuthuoc;

  @PrePersist
  public void prePersist() {
    this.id = this.id == null || this.id.equals("")? UUID.randomUUID().toString(): null;
  }
}
