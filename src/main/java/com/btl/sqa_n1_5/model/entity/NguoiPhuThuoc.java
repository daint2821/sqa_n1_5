package com.btl.sqa_n1_5.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.PrePersist;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NguoiPhuThuoc {

  @Id
  private String id;
  @Column(name = "nguoidongthueid")
  private String nguoidongthueid;
  private String hovaten;
  private String CCCD;

  @PrePersist
  public void prePersist() {
    this.id = this.id == null || this.id.equals("")? UUID.randomUUID().toString(): null;
  }
}
