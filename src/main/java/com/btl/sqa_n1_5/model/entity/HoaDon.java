package com.btl.sqa_n1_5.model.entity;

import com.btl.sqa_n1_5.model.dto.HoaDonDTO;
import java.util.Date;
import java.util.UUID;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HoaDon {

  @Id
  private String id;
  private String nguoidongthueId;
  private Long TNCT;
  private Long TNTT;
  private Long thuedanop;
  @PrePersist
  public void prePersist() {
    this.id = this.id == null || this.id.equals("")? UUID.randomUUID().toString(): null;
  }
}
