package com.btl.sqa_n1_5.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import java.util.Date;
import java.util.UUID;

import lombok.Data;

//@Entity
@Data
public class NguoiDongThue {
//  @Id
  private String ThanhvienId;
  private String masothue;
  private String coquanthue;
  private String hovaten;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
  private Date ngaysinh;
  private String dienthoai;
  private String email;
  private String gioitinh;
  private String quoctich;
  private String diachiThuongtru;
  private String diachiHientai;
  @JsonProperty("cccd")
  private Integer CCCDId;
  private Integer taxStatus;
  @PrePersist
  public void prePersist() {
    this.ThanhvienId = this.ThanhvienId == null || this.ThanhvienId.equals("")? UUID.randomUUID().toString(): null;
  }
}
