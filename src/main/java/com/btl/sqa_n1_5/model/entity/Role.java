package com.btl.sqa_n1_5.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import java.util.List;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="roles")
@ToString
public class Role
{
  @Id
  @Column(name = "id")
  private String id;

  @Column(nullable=false, unique=true)
  private String name;

  @ManyToMany(mappedBy="roles")
  private List<Thanhvien> users;

  @PrePersist
  public void prePersist() {
    this.id = this.id == null || this.id.equals("")? UUID.randomUUID().toString(): null;
  }
}
