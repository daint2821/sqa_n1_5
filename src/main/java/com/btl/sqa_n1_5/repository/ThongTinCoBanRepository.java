package com.btl.sqa_n1_5.repository;

import com.btl.sqa_n1_5.model.entity.ThongTinCoBan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ThongTinCoBanRepository extends JpaRepository<ThongTinCoBan, String> {
  Optional<ThongTinCoBan> findByNguoiDongThueId(String id);
}
