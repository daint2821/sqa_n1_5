package com.btl.sqa_n1_5.repository;

import com.btl.sqa_n1_5.model.entity.NguoiPhuThuoc;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NguoiPhuThuocRepository extends JpaRepository<NguoiPhuThuoc, String> {
  List<NguoiPhuThuoc> findAllByNguoidongthueid(String id);

}
