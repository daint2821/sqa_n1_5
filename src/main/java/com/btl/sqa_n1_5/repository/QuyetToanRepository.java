package com.btl.sqa_n1_5.repository;

import com.btl.sqa_n1_5.model.entity.ThongTinThue;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface QuyetToanRepository extends JpaRepository<ThongTinThue, String> {
  Optional<ThongTinThue> findByNguoiDongThueId(String id);

}
